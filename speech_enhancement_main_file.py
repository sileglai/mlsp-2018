#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright: Inria
Author: Simon Leglaive
Contact: simon.leglaive@inria.fr
Date: May 2018
License: see LICENSE.txt
"""
import numpy as np
import librosa
import os
import pickle
from VAE import VAE, Decoder, Encoder
from MCEM_algo import MCEM_algo

#%% Path to directories

# Directory to load the mixture
data_dir = 'audio'

# Directory to load the VAE model
vae_dir = 'training_results/latent_dim=64-2018-03-27-11h45'


#%% Parameters

fs = int(16e3) # Sampling rate
eps = np.finfo(float).eps # machine epsilon

# STFT parameters
wlen_sec = 64e-3 # STFT window length in seconds
hop_percent = 0.25  # hop size as a percentage of the window length
zp_percent=0
wlen = wlen_sec*fs # window length of 64 ms
wlen = np.int(np.power(2, np.ceil(np.log2(wlen)))) # next power of 2
hop = np.int(hop_percent*wlen) # hop size
nfft = wlen + zp_percent*wlen # number of points of the discrete Fourier transform
win = np.sin(np.arange(.5,wlen-.5+1)/wlen*np.pi); # sine analysis window

# Algorithm parameters
niter_MCEM = 200
niter_MH = 40
burnin = 30
var_MH = 0.01
K = 10 # NMF rank for the noise model
tol = 1e-4

#%% Load mixture and compute STFT

mix_file = os.path.join(data_dir, 'mix.wav')
x, fs_x = librosa.load(mix_file, sr=None) # Load wav file without resampling
x = x/np.max(np.abs(x))
if fs != fs_x:
    raise ValueError('Unexpected sampling rate for the mixture signal')
T_orig = len(x)
x_pad = librosa.util.fix_length(x, T_orig + wlen // 2)
X = librosa.stft(x, n_fft=nfft, hop_length=hop, win_length=wlen, window=win)
F, N = X.shape

#%% Load VAE

# Load parameters
parms_pckl_file = os.path.join(vae_dir, 'parameters.pckl')
dic_params = pickle.load( open( parms_pckl_file, "rb" ))
for key, value in dic_params.items():
    if isinstance(value, str):
        exec("%s = '%s'" % (key, value))
    else:
        exec("%s = %s" % (key, value))


verbose_data = True
verbose_vae = 1
display_network = False

# Build VAE
vae = VAE(input_dim=input_dim, latent_dim=latent_dim,
          intermediate_dim=intermediate_dim, batch_size=1,
          activation=activation)
vae.build()

# Load saved weights
weights_file = os.path.join(vae_dir, 'saved_weights.h5')
vae.load_weights(weights_file)

# Build decoder
decoder = Decoder(batch_size=N)
decoder.build(vae)

# Build encoder
encoder = Encoder(batch_size=N)
encoder.build(vae)

del vae

#%% Main loop

# Initialize noise model parameters
np.random.seed(0)
W_init = np.maximum(np.random.rand(F,K), eps)
H_init = np.maximum(np.random.rand(K, N), eps)

# Compute the first latent variable sample
latent_mean, latent_log_var = encoder.encode(np.abs(X).T**2)
Z_init = latent_mean.T

# Instanciate the MCEM algo
mcem_algo = MCEM_algo(X=X, W=W_init, H=H_init, Z=Z_init, decoder=decoder,
                      niter_MCEM=niter_MCEM, niter_MH=niter_MH, burnin=burnin,
                      var_MH=var_MH)

# Run the MCEM algo
cost, niter_final = mcem_algo.run(hop=hop, wlen=wlen, win=win, tol=tol)
cost = cost[0:niter_final]

# Separate the sources from the estimated parameters
mcem_algo.separate( niter_MH=100, burnin=75)

s_hat = librosa.istft(stft_matrix=mcem_algo.S_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)
n_hat = librosa.istft(stft_matrix=mcem_algo.N_hat, hop_length=hop,
                      win_length=wlen, window=win, length=T_orig)

# Save estimated wav files
librosa.output.write_wav(os.path.join(data_dir, 'speech_est.wav'), s_hat, fs)
librosa.output.write_wav(os.path.join(data_dir,'noise_est.wav'), n_hat, fs)
