# A Variance Modeling Framework Based on Variational Autoencoders for Speech Enhancement

This repository contains the Python implementation of the speech enhancement method proposed in the paper whose reference is below. We provide:

* the Keras implementation for training the supervised speech model, which is based on a variational autoencoder;
* the Keras models trained on the [TIMIT](https://catalog.ldc.upenn.edu/LDC93S1) database;
* the implementation of the proposed Monte Carlo expectation-maximization algorithm for performing speech enhancement.

## Reference

**Title:**  A variance modeling framework based on variational autoencoders for speech enhancement

**Authors:** Simon Leglaive, Laurent Girin, Radu Horaud

**Conference:** 2018 IEEE International Workshop on Machine Learning for Signal Processing

**Article:** [here](https://hal.inria.fr/hal-01832826v1/document)

**Supporting document:** [here](https://sleglaive.github.io/documents/sup_doc_MLSP18.pdf)

**Bibtex:** [here](https://hal.inria.fr/hal-01832826v1/bibtex)

## Demos

Audio examples are available [here](https://team.inria.fr/perception/research/ieee-mlsp-2018/).

## Repository Content

**Root directory**

* VAE.py - Contains classes related to variational autoencoders, with several methods such as for training, encoding, decoding, etc.
* training_main_file.py - Main script for training the variational autoencoder.
* data_tools.py - Contains functions for computing the training data.
* MCEM_algo.py - Monte Carlo expectation-maximization algorithm.
* speech_enhancement_main_file.py - Main script for enhancing a noisy speech signal.
* test_dataset_info.csv - CSV file describing how the 168 noisy mixtures used in the evaluation can be created from the [TIMIT](https://catalog.ldc.upenn.edu/LDC93S1) and [DEMAND](https://zenodo.org/record/1227121#.W8S9D3UzZhE) databases.

**Audio**

* mix.wav - Noisy speech signal
* noise_est.wav - Noise estimate
* speech_est.wav - Speech estimate

**training_results**

Each subfolder corresponds to a different choice for the dimension of the latent random vector involved in the variational autoencoder (8, 16, 32, 64 or 128).

* saved_weights.h5 - Weights of the network after training.
* parameters.txt - Network and training parameters in a text file (see training_main_file.py and data_tools.py).
* parameters.pckl - Network and training parameters in a pickle file (see training_main_file.py and data_tools.py).

## Conda Requirements

Please refer to the two YAML files containing the conda environements used for training ('conda-environment-gpu-training.yml') and testing ('conda-environment-test.yml').

## License

See LICENSE.txt

## Acknowledgement

We would like to thank Manuel Pariente for pointing out a very effective way of speeding up this algorithm. The current implementation of the algorithm is faster than the one used to perform the experiments in the above-mentioned MLSP 2018 article.
